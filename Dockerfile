FROM python:3.6.8

COPY . /usr/src/app
WORKDIR /usr/src/app

RUN pip install Flask


EXPOSE 5000
CMD [ "python" , "app.py"]

